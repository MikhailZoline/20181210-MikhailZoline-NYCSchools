//
//  MainTableViewCell.swift
//  20181210-MikhailZoline-NYCSchools
//
//  Created by Mikhail Zoline on 12/10/18.
//  Copyright © 2018 MZ. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {

    @IBOutlet var schoolName: UILabel!
    
    @IBOutlet var schoolOverview: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
