//
//  MainViewController.swift
//  20181210-MikhailZoline-NYCSchools
//
//  Created by Mikhail Zoline on 12/10/18.
//  Copyright © 2018 MZ. All rights reserved.
//  TO DO : Implement the Coordinator pattern
//  which will be in charge of the flow of control
//  The view controllers then are completely
//  independent and have no knowledge of the context
//  in which they are used, improving their reusability
//  & testing.
import UIKit

class MainViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let networkManager: NetworkManager = NetworkManager.sharedInstance
    
    var schoolArray: [SchoolSummaryStruct] = [SchoolSummaryStruct]()
    
    var operationQueue: OperationQueue = OperationQueue()
    
    @IBOutlet var tableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 500
        self.tableView.tableFooterView = UIView()
        self.schoolListUpdated()
    }
    
    // MARK: - Data structure initialisation from API requests
    
    func schoolListUpdated() {
        operationQueue.qualityOfService = .default
        let getSchoolListOperation: BlockOperation = BlockOperation(block: {[weak self] in
            self!.networkManager.getList(
                jsonType: .school,
                completion: { (schoolArray, error) in
                    if ((error) != nil){
                        fatalError("Failed to Initialize JSON object \(error!.description)")
                    }
                    OperationQueue.main.addOperation { [weak self] in
                        self?.schoolArray = schoolArray as! [SchoolSummaryStruct]
                        self?.tableView.reloadData()
                    }
            })
        })
        getSchoolListOperation.completionBlock = {[weak self] in
            self?.satListUpdated()
        }
        operationQueue.addOperation(getSchoolListOperation)
    }
    
    func satListUpdated() {
        
        let getSchoolSATOperation = {[weak self]  in
            self!.networkManager.getList(jsonType: .sat, completion: { (satArray,error) in
                if ((error) != nil) {
                    fatalError("Failed to Initialize JSON object \(error!.description)")
                }

                // By now I have satArray which contains the SAT records from API
                // I filter by school_name and dbn here
                // If school_name or dbn of the element from schoolArray is found in the array
                // from SAT databse,
                // I append SAT record to the corresponding element of the schoolArray
                // Note: num of SAT records found by school_name or dbn is 372 ot of 440
                self?.schoolArray = (self?.schoolArray.map({ (schoolStruct) -> SchoolSummaryStruct in
                    var tmpSchoolStructure = schoolStruct
                    var tmpSatStructure: SchoolSATStruct? = nil
                    
                    if(((satArray as! [SchoolSATStruct]).contains(where: { (satStruct) -> Bool in
                        if( (satStruct.school_name?.localizedUppercase
                                == schoolStruct.school_name?.localizedUppercase) ||
                            (satStruct.dbn?.localizedUppercase
                                == schoolStruct.dbn?.localizedUppercase)){
                    
                            tmpSatStructure = satStruct
                            return true
                        }
                        return false
                    }))){
                        tmpSchoolStructure.school_sat = tmpSatStructure
                    }
                    else{
                        tmpSchoolStructure.school_sat = nil
                    }
                    return tmpSchoolStructure
                }))!
    
            })
        }
        operationQueue.addOperation(getSchoolSATOperation)
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView
        : UITableView, numberOfRowsInSection section
        : Int) -> Int {
        return schoolArray.count
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView
        : UITableView, cellForRowAt indexPath
        : IndexPath)
        ->UITableViewCell{
        guard  let cell: MainTableViewCell =
            tableView.dequeueReusableCell(withIdentifier:
                "MainTableViewCell", for: indexPath) as? MainTableViewCell
                else {
                    fatalError("Failed to Initialize Main Table View Cell")
            }
            configureCell(cell: cell, atIndexPath: indexPath)
            return cell
    }
    
    func configureCell(cell
        : MainTableViewCell, atIndexPath indexPath
        : IndexPath){
        guard let schoolSummary : SchoolSummaryStruct =
            self.schoolArray[indexPath.row] as SchoolSummaryStruct?
        else {
            fatalError("Failed to Initialize Main Table View Cell")
        }
        
        cell.schoolName.text =
            (schoolSummary.name) + " in " +
            "\(schoolSummary.neighborhood)"
        
        cell.schoolOverview.text = (String(utf8String
            : (schoolSummary.overview_paragraph?.components(separatedBy
                : CharacterSet(charactersIn: ".?"))
                .first?.cString(using: String.Encoding.utf8))!)?.replacingOccurrences(of: "Â", with: ""))! +  ( ((schoolSummary.overview_paragraph?.firstIndex(of: "?")) != nil) ? "?" : ".")
    }
    
    func tableView(_ tableView
        : UITableView, canEditRowAt indexPath
        : IndexPath)
        ->Bool {
            return false
    }
    
    
    // MARK: - Added functionnality of sorting by neighborhoor or by school name
    
    @IBAction func sortBySchoolName(_ sender : Any) {
        let operation = BlockOperation(block: {[weak self] in
            self?.schoolArray.sort(by: { (school1, school2) -> Bool in
                return school1.name < school2.name
            } )
        } )
        operation.completionBlock = { OperationQueue.main.addOperation { [weak self] in
            self?.tableView.reloadData()
            }
        }
        OperationQueue.main.addOperation(operation)
    }
    
    @IBAction func sortByNeighborhood(_ sender : Any) {
        let operation = BlockOperation(block: {[weak self] in
            self?.schoolArray.sort(by: { (school1, school2) -> Bool in
                return school1.neighborhood < school2.neighborhood
            } )
        } )
        operation.completionBlock = { OperationQueue.main.addOperation { [weak self] in
            self?.tableView.reloadData()
            }
        }
        OperationQueue.main.addOperation(operation)
    }
    
     // MARK: - Navigation
    
    override func prepare(for segue:
        UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "ShowDetailController") {
            let indexPath : IndexPath = self.tableView.indexPathForSelectedRow!
            let schoolSummary : SchoolSummaryStruct? = self.schoolArray[indexPath.row]
            // Pass the selected book to the new view controller.
            let detailViewController : DetailViewController = (segue.destination as! DetailViewController)
            detailViewController.schoolSummaryDetail = schoolSummary!
        }
    }
 
}
