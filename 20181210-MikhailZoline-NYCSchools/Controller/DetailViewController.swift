//
//  DetailViewController.swift
//  20181210-MikhailZoline-NYCSchools
//
//  Created by Mikhail Zoline on 12/10/18.
//  Copyright © 2018 MZ. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    
    var schoolSummaryDetail: SchoolSummaryStruct?  = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorColor = UIColor.clear
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 1 ?
            self.schoolSummaryDetail?.school_sat == nil || (self.schoolSummaryDetail?.school_sat != nil && self.schoolSummaryDetail?.school_sat!.sat_critical_reading_avg_score == "s") ?
        " Record is NOT FOUND in SAT DB"
        : " Average SAT Scores"
        : nil
    }
    
    // I reuse the MainViewControllerCell here, it's automatically expanded
    // to accomodate the whole overview
    // the SAT scores are rendered in second section
    func tableView(_ tableView
        : UITableView, cellForRowAt indexPath
        : IndexPath)
        ->UITableViewCell{
            var cell: UITableViewCell? = nil
            if indexPath.section == 0 {// overview section
                cell = tableView.dequeueReusableCell(withIdentifier:
                        "MainTableViewCell", for: indexPath) as! MainTableViewCell
                    if cell == nil {
                        fatalError("Failed to Initialize Detail Table View Cell")
                }
            }
            else{// score section
                cell = tableView.dequeueReusableCell(withIdentifier:
                    "DetailTableViewCell", for: indexPath) as! DetailTableViewCell
                if cell == nil {
                    fatalError("Failed to Initialize Detail Table View Cell")
                }
            }
            configureCell(cell: (cell)! , atIndexPath: indexPath)
            return (cell)!
    }
    
    func configureCell(cell
        : UITableViewCell, atIndexPath indexPath
        : IndexPath){
        if cell is MainTableViewCell {
            guard let schoolSummary : SchoolSummaryStruct =
                schoolSummaryDetail as SchoolSummaryStruct?
                else {
                    fatalError("Failed to Initialize Detail Table View Cell")
            }
            
            (cell as! MainTableViewCell).schoolName.text =
                (schoolSummary.name) + " in " +
            "\(schoolSummary.neighborhood)"
            
            (cell as! MainTableViewCell).schoolOverview.text = String(utf8String
                : (schoolSummary.overview_paragraph?.cString(using
                : String.Encoding.utf8))!)?.replacingOccurrences(of
                : "Â", with: "")
        }
        else{
                let schoolSat : SchoolSATStruct? =
                schoolSummaryDetail?.school_sat as SchoolSATStruct?
 
            (cell as! DetailTableViewCell).readingScore.text = schoolSat == nil || schoolSat?.sat_critical_reading_avg_score == "s" ? "" :  schoolSat?.sat_critical_reading_avg_score
            (cell as! DetailTableViewCell).writingScore.text = schoolSat == nil || schoolSat?.sat_writing_avg_score == "s" ? "" : schoolSat?.sat_writing_avg_score
            (cell as! DetailTableViewCell).mathScore.text = schoolSat == nil || schoolSat?.sat_math_avg_score == "s" ? "" : schoolSat?.sat_math_avg_score
        }
    }
    
    func tableView(_ tableView
        : UITableView, canEditRowAt indexPath
        : IndexPath)
        ->Bool {
            return false
    }
    
}
