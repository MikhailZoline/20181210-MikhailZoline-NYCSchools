//
//  SchoolModel.swift
//  20181210-MikhailZoline-NYCSchools
//
//  Created by Mikhail Zoline on 12/10/18.
//  Copyright © 2018 MZ. All rights reserved.
//

import Foundation

struct DOEHighSchoolJsonResponse: Codable{
    var schools: [SchoolSummaryStruct]
}

// Codable comes in pretty handy since swift 4
struct SchoolSummaryStruct: Codable {
    var school_name: String?
    var boro: String?
    var overview_paragraph: String?
    var dbn: String?
    var school_sat: SchoolSATStruct?
}

extension SchoolSummaryStruct{
    var neighborhood: String {
        switch boro{
        case "X":
            return "Bronx"
        case "M":
            return "Manhattan"
        case "Q":
            return "Queens"
        case "R":
            return "Staten Island"
        case "K":
            return "Brooklyn"
        default:
            return ""
        }
    }
    
    var name : String {
        return school_name!
    }
}

struct SchoolSATStruct: Codable {
    var sat_critical_reading_avg_score: String?
    var sat_math_avg_score: String?
    var sat_writing_avg_score: String?
    var school_name: String?
    var dbn: String?
}


extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
