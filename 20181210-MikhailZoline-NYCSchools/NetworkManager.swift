//
//  NetworkManager.swift
//  20181210-MikhailZoline-NYCSchools
//
//  Created by Mikhail Zoline on 12/10/18.
//  Copyright © 2018 MZ. All rights reserved.
//
// There's really no point in using of Alamo and swityJson anymore,
// with NSURLSession and Decodable being so good and simple
// No need to import a giant library that just adds to the App bundle

import Foundation
import CoreLocation
protocol NetworkManagerDelegate: class {
    func schoolListUpdated()
}

enum NetworkResponse:String {
    case success
    case authenticationError = "You need to be authenticated first."
    case badRequest = "Bad request"
    case outdated = "The url you requested is outdated."
    case failed = "Network request failed."
    case noData = "Response returned with no data to decode."
    case unableToDecode = "We could not decode the response."
}

enum Result<String>{
    case success
    case failure(String)
}

fileprivate func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String>{
    switch response.statusCode {
    case 200...299: return .success
    case 401...500: return .failure(NetworkResponse.authenticationError.rawValue)
    case 501...599: return .failure(NetworkResponse.badRequest.rawValue)
    case 600: return .failure(NetworkResponse.outdated.rawValue)
    default: return .failure(NetworkResponse.failed.rawValue)
    }
}

enum JsonType{
    case school
    case sat
}
public typealias NetworkRequestCompletion = (_ data: Data?,_ response: URLResponse?,_ error: Error?)->()

class NetworkManager {
    //invariants
    static let APIToken: String = "9j2JcOE2eJYFjbEBoDKmkt6Bp"
    static let APIHeaderTokenFiled = "X-App-Token"
    static let APIResponseContentType = "application/json"
    static let APIHeaderContentTypeField = "Accept"
    static let SchoolListAPIURL: String = "https://data.cityofnewyork.us/resource/"
    static let SchoolListAPIParameters: String = "97mf-9njv.json?$select=school_name,boro,overview_paragraph,dbn"
    static let SchoolSATAPIParameters: String =
    "734v-jeq5.json?$select=school_name,sat_critical_reading_avg_score,sat_math_avg_score,sat_writing_avg_score,dbn"

    private
    var task : URLSessionTask?
    
    private init() {}
    static let sharedInstance = NetworkManager()
    // generic URLSession request
    func request( stringURL: String, completion: @escaping NetworkRequestCompletion) {
        
        let session = URLSession.shared
        
        guard let url = URL(string: stringURL) else{
            fatalError("Failed to create URL with \(stringURL)" )
        }
        var request = URLRequest(url:url)
        request.httpMethod = "GET"
        request.setValue(NetworkManager.APIToken, forHTTPHeaderField: NetworkManager.APIHeaderTokenFiled)
        request.setValue(NetworkManager.APIHeaderContentTypeField, forHTTPHeaderField: NetworkManager.APIResponseContentType)
        self.task = session.dataTask(with: request, completionHandler: { data, response, error in
            completion(data, response, error)
        })
        
        self.task?.resume()
    }
    
    // get json array of schools or school SAT stats, refer to SchoolModel
    func getList(jsonType: JsonType, completion: @escaping (_ summary: [Any]?,_ error: String?)->()){
        
        let stringURL
            : String =
            NetworkManager.SchoolListAPIURL +
                (jsonType == .school ? NetworkManager.SchoolListAPIParameters
                    : NetworkManager.SchoolSATAPIParameters)
        
        self.request(
            stringURL
            : stringURL, completion
            : {
                (data, response, error) in if error != nil {
                    completion(
                        nil,
                        "Please check your network connection.")
                }
                if
                let response = response as? HTTPURLResponse {
                    let result = handleNetworkResponse(response)
                    switch result {
                        case.success:
                            guard let responseData = data else {
                                completion(nil,NetworkResponse.noData.rawValue)
                                return
                            }
                            do {
                                if jsonType == .school {
                                    let responseJSON
                                        : [SchoolSummaryStruct] =
                                        try
                                            JSONDecoder()
                                                .decode(
                                                    [SchoolSummaryStruct]
                                                        .self,
                                                    from
                                                    : responseData)
                                    
                                    OperationQueue
                                        .main
                                        .addOperation(
                                            {completion(
                                                responseJSON,
                                                nil)})
                                }
                                else {
                                    let responseJSON
                                        : [SchoolSATStruct] = try
                                            JSONDecoder()
                                                .decode(
                                                    [SchoolSATStruct]
                                                        .self,
                                                    from
                                                    : responseData)
                                    OperationQueue
                                        .main.addOperation(
                                            {completion(
                                                responseJSON,
                                                nil)})
                                }
                            }
                            catch {
                                print("Failed to Initialize JSON object \(error)")
                                completion(nil,
                                           NetworkResponse
                                            .unableToDecode
                                            .rawValue)
                            }
                        case.failure(let networkFailureError):
                            completion(nil, networkFailureError)
                    }
                }
        })
    }
}
