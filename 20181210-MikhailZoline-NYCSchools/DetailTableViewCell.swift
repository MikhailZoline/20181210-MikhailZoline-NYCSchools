//
//  DetailTableViewCell.swift
//  20181210-MikhailZoline-NYCSchools
//
//  Created by Mikhail Zoline on 12/10/18.
//  Copyright © 2018 MZ. All rights reserved.
//

import UIKit

class DetailTableViewCell: UITableViewCell {
    
    @IBOutlet var readingScore: UILabel!
    
    @IBOutlet var writingScore: UILabel!
    
    @IBOutlet var mathScore: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
