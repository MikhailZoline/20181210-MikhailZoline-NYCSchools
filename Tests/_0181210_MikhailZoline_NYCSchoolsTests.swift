//
//  _0181210_MikhailZoline_NYCSchoolsTests.swift
//  20181210-MikhailZoline-NYCSchoolsTests
//
//  Created by Mikhail Zoline on 12/10/18.
//  Copyright © 2018 MZ. All rights reserved.
//

import XCTest
@testable import _0181210_MikhailZoline_NYCSchools

class _0181210_MikhailZoline_NYCSchoolsTests: XCTestCase {

    let networkManager: NetworkManager = NetworkManager.sharedInstance
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        XCTAssertNotNil(networkManager)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGetSchoolList() {
        let expectation:  XCTestExpectation = XCTestExpectation(description: "Failed To GetSchoolList")
        networkManager.getList(jsonType: .school) { (schoolArray, errorString) in
            XCTAssertNil(errorString)
            XCTAssertNotNil(schoolArray)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10)
    }
    
    func testGetSATList() {
        let expectation:  XCTestExpectation = XCTestExpectation(description: "Failed To GetSATList")
        networkManager.getList(jsonType: .sat) { (satArray, errorString) in
            XCTAssertNil(errorString)
            XCTAssertNotNil(satArray)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10)
    }
    
    // Could measure a benchmark of network request here
    func testPerformanceExample() {
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
